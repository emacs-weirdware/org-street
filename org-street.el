;;; org-street.el --- Physical addresses for Org   -*- lexical-binding: t; -*-

;; Copyright (C) 2019, 2022  Ian Eure

;; Author: Ian Eure <public@lowbar.fyi>
;; Version: 0.7.1
;; URL: https://codeberg.org/emacs-weirdware/org-street
;; Package-Requires: ((emacs "25.1") (nominatim "0.9.3"))
;; Keywords: lisp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Org Street is an extension for Org Mode for turning the names of
;; places into a `LOCATION' property containing their address.  Given
;; some freeform text approximately describing a location, it geocodes it
;; with OpenStreetMap’s Nominatim API to determine a canonical location.
;; If Nominatim returns multiple locations, a list is displayed to choose
;; from.
;;
;; Having a `LOCATION' is helpful; if you export your Org Agenda to iCal
;; files, the location becomes the address of the calendar entry.
;;
;; Usage
;; -----
;;
;; Place point inside an Org entry, then run:
;;
;; `M-x org-street-set-location RET'
;;
;; Then enter some text and press `RET' again.
;;

;;; Code:

(require 'nominatim)

;;; Code:

(defgroup org-street nil
  "Settings for org-street")

(defcustom org-street-properties
  '(("LOCATION" . nominatim-printable-oneline))
  "An alist of Org properties and functions for `org-street-set-location'.
These functions should accept one argument (a nominatim-location)
and return a string.  When `org-street-set-location' has found a
location, the functions in this alist are each called on said
location and the values they return are assigned to the
corresponding Org properties.

As an example, this can be used to store coordinates as well as
the street address, or to change the property in which the street
address is stored.  An example value may be seen below:

`'((\"ADDRESS\" . nominatim-printable-oneline)
 (\"LATITUDE\" . (lambda (location) (cdr (assoc 'lat location))))
 (\"LONGITUDE\" . (lambda (location) (cdr (assoc 'lon location)))))'"
  :type '(alist :key-type string :value-type function)
  :group 'org-street)

(defun org-street--set-location (location)
  "Derive and assign Org property values based on LOCATION.

The functions in this `org-street-properties' are each called on
LOCATION and the values they return are assigned to the
corresponding Org properties."
  (mapcar (lambda (pair)
            (org-set-property (car pair)
                              (funcall (cdr pair) location)))
          org-street-properties))

(defun org-street--completing-read (prompt locations)
  "Let the user pick a location out of a sequence of locations.

The user is presented with the addresses of all available
locations using `completing-read'.  If the user picks one of
them, the associated location is returned.  If the user does not
pick one of the suggested addresses, `nil' is returned."
  (and-let* ((alist (mapcar (lambda (location)
                              (cons (nominatim-printable-oneline location)
                                    location))
                            locations))
             (key (completing-read prompt
                                   alist
                                   (lambda (&rest args) t)
                                   t)))
    (assoc key
           alist
           #'string-equal)))

(defun org-street--choose (prompt locations)
  "Choose one location out of an array of locations.

If the array is empty, return `nil'.
If the array contains exactly one location, return it.
If the array contains several locations, let the user pick one.
If the user does not pick one of the offered locations, return `nil'."
  (let ((n (length locations)))
    (cond ((= n 0) nil)
          ((= n 1) (aref locations 0))
          (t (org-street--completing-read prompt locations)))))

 ;; User-serviceable parts

(defun org-street-set-location (text)
  "Set Org properties of the current entry based on the address of TEXT.

Nominatim is used to look up TEXT.  If no location is returned,
an error is signalled.  If a single location is returned, this
location is chosen.  If several locations are returned, a list is
displayed to choose from.

Once a location associated with TEXT is chosen, the
property-function pairs from `org-street-properties' are used to
derive Org property values from said location and to assign these
values to the corresponding Org properties of the current entry."
  (interactive (list (read-string "Location: ")))
  (let ((location (org-street--choose "Location: "
                                      (nominatim-geocode text))))
    (if location
        (org-street--set-location location)
      (error "No matches for `%s'" text))))

(provide 'org-street)

;;; org-street.el ends here
